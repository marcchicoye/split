﻿using System;

namespace Split
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sentenceToSplit = getSentenceToSplit();


            Splitter splitter = new Splitter();
            string[] wordsSplitted = splitter.toSplit(sentenceToSplit[0], sentenceToSplit[1][0]);

            display(wordsSplitted);
        }

        static string[] getSentenceToSplit()
        {
            System.Console.WriteLine("Write your delimited sentence: ");
            string wordsToSplit = System.Console.ReadLine();
            System.Console.WriteLine("What is the delimiter character: ");
            ConsoleKeyInfo delimiter = System.Console.ReadKey();
            System.Console.WriteLine();
            string[] info = new string[2];
            info[0] = wordsToSplit;
            info[1] = delimiter.KeyChar.ToString();
            return info;
        }

        static void display(string[] arrayToDisplay)
        {
            for (int i = 0; i < arrayToDisplay.Length; i++)
            {
                System.Console.WriteLine(arrayToDisplay[i]);
            }
        }
    }

    public class Splitter
    {

        public string[] toSplit(string wordsToSplit, char delimiter)
        {
            if (wordsToSplit.Length == 0)
            {
                return new string[] { };
            }

            string[] tempWordsSplitted = new string[this.countDelimiter(wordsToSplit, delimiter) + 1];

            string tempWord = "";
            int index = 0;

            foreach (char character in wordsToSplit)
            {
                if(character == delimiter)
                {
                    tempWordsSplitted[index] = tempWord;
                    tempWord = "";
                    ++index;
                }
                else
                {
                    tempWord += character;
                }
            }

            tempWordsSplitted[index] = tempWord;

            return tempWordsSplitted;
        }


        public int countDelimiter(string sampleStringWithDelimiters, char delimiter)
        {
            int numberOfDelimiter = 0;
            foreach (char c in sampleStringWithDelimiters)
            {
                if (c == delimiter)
                    ++numberOfDelimiter;
            }
            return numberOfDelimiter;
        }

    }
}
