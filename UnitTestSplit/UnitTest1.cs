using Microsoft.VisualStudio.TestTools.UnitTesting;
using Split;

namespace UnitTestSplit
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestToSplit_testEmptyTokens_emptyTokenIsReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] { };
            string inputString = "";
            char inputDelimiter = ',';
            


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
        }

        [TestMethod]
        public void TestToSplit_testOneTokenStringNoDelimiter_OneIsReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] {"allo"};
            string inputString = "allo";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
        }

        [TestMethod]
        public void TestToSplit_testOneTokenStringWithDelimiterAtBegin_oneEmptyAndOneStringTokensAreReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] {"allo","" };
            string inputString = "allo,";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
            Assert.AreEqual(expectedResult[1], actualResult[1]);
        }

        [TestMethod]
        public void TestToSplit_testOneTokenStringWithDelimiterAtEnd_oneStringAndOneEmptyTokensAreReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] { "", "allo" };
            string inputString = ",allo";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
            Assert.AreEqual(expectedResult[1], actualResult[1]);
        }

        [TestMethod]
        public void TestToSplit_testOneTokenStringWithDelimiterAtBeginAndEnd_OneEmptyAndOneStringAndOneEmptyTokensAreReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] { "", "allo","" };
            string inputString = ",allo,";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
            Assert.AreEqual(expectedResult[1], actualResult[1]);
            Assert.AreEqual(expectedResult[2], actualResult[2]);
        }

        [TestMethod]
        public void TestToSplit_testWithOneDelimiterOnly_TwoEmptyTokensAreReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] { "", "" };
            string inputString = ",";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
            Assert.AreEqual(expectedResult[1], actualResult[1]);
        }

        [TestMethod]
        public void TestToSplit_testTwoDelimitersOnly_ThreeEmptyTokensAreReturned()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string[] expectedResult = new string[] { "", "", "" };
            string inputString = ",,";
            char inputDelimiter = ',';


            // Run the method under test:
            string[] actualResult = splitter.toSplit(inputString, inputDelimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult.Length, actualResult.Length);
            Assert.AreEqual(expectedResult[0], actualResult[0]);
            Assert.AreEqual(expectedResult[1], actualResult[1]);
            Assert.AreEqual(expectedResult[2], actualResult[2]);
        }



        [TestMethod]
        public void TestToCount_testWithStringNoDelimiter_returnZero()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = "one two three four";
            char delimiter = ',';
            int expectedResult = 0;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testWithEmptyStringNoDelimiter_returnZero()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = "";
            char delimiter = ',';
            int expectedResult = 0;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testWithEmptyStringOneDelimiter_returnOne()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = ",";
            char delimiter = ',';
            int expectedResult = 1;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testWithEmptyStringTwoDelimiter_returnTwo()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = ",,";
            char delimiter = ',';
            int expectedResult = 2;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testWithEmptyStringThreeDelimiter_returnThree()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = ",,,";
            char delimiter = ',';
            int expectedResult = 3;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testOneStringOneDelimiterAtBegin_returnOne()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = ",allo";
            char delimiter = ',';
            int expectedResult = 1;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testOneStringOneDelimiterAtEnd_returnOne()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = "allo,";
            char delimiter = ',';
            int expectedResult = 1;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestToCount_testOneStringWithDelimiterAtBeginAndEnd_returnTwo()
        {
            // Create an instance to test:
            var splitter = new Splitter();

            // Define a test input and output value
            string sentenceToSplit = ",allo,";
            char delimiter = ',';
            int expectedResult = 2;

            // Run the method under test:
            int actualResult = splitter.countDelimiter(sentenceToSplit, delimiter);

            // Verify the result:
            Assert.AreEqual(expectedResult, actualResult);
        }

    }
}
